# MongoDB Playground
A collection of exercises to introduce myself on MongoDB.

To work with the dataset given just run

`$ mongorestore --db [your db name] [path/to/dump/folder/]` 

and get data from games.json and put it in two collections, one for videogames
and the other for boardgames.