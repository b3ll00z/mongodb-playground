import json
import pymongo
import os
from pprint import pprint

def connect():
	"""Connect to a videogames database in localhost"""
	
	return pymongo.MongoClient('localhost')['videogames']


def __pretty_print(cursor):
	"""Iterate over the cursor printing every row as it is"""
	
	for doc in cursor:
		pprint(str(doc))


def __simple_print(cursor):
	
	for doc in cursor:
		print(str(doc))


def q1(db, count=None):
	"""Find videogames that can be played by 2 people"""
	query_object = {'max_players': {'$gte': 2}}
	
	if count:
		print(db.videogames.find(query_object).count())
	else:
		__pretty_print(db.videogames.find(query_object))


def q2(db, count=None):
	"""Find title of boardgames that can be played by 2 people"""
	ob1 = {'max_players': {'$gte': 2}}
	ob2 = {'_id': 0, 'title': 1}

   	if count:
       	print(db.boardgames.find(ob1, ob2).count())
   	else:    	
		__simple_print(db.boardgames.find(ob1, ob2))


def q3(db, count=None):
	"""Find title and year of boardgames that can be played by 2 people sorted by year
descending and title ascending"""
	
	ob1 = {'max_players': {'$gte': 2}}
	ob2 = {'_id': 0, 'title': 1, 'year': -1}
	
   	if count:
       	print(db.boardgames.find(ob1, ob2).count())
   	else:
		__simple_print(db.boardgames.find(ob1, ob2))


def q4(db, count=None):
	"""Find titles videogames of category Action"""

   	ob1 = {'categories': {'$elemMatch': {'$eq': 'Action'}}}
   	ob2 = {'_id': 0, 'title': 1}
        
   	if count:
   		print(db.videogames.find(ob1, ob2).count())
   	else:    
       	__simple_print(db.videogames.find(ob1, ob2))


def q5(db, count=None):
	"""Find titles, year, and categories of videogames either of category Action or Adventure"""
	
   	ob1 = {'categories': {'$in': ['Action', 'Adventure']}}
   	ob2 = {'_id': 0, 'title': 1, 'year': 1, 'categories': 1}

   	if count:
       	print(db.videogames.find(ob1, ob2).count())
   	else:    
       	__simple_print(db.videogames.find(ob1, ob2))


def q6(db, count=None):
	"""Find title and categories of videogames that are of type Action AND Adventure"""
	
   	ob1 = {'$and': [{'categories': {'$elemMatch': {'$eq': 'Action'}}}, 
			{'categories': {'$elemMatch': {'$eq': 'Adventure'}}}]}
   	ob2 = {'_id': 0, 'title': 1, 'year': 1, 'categories': 1}

   	if count:
       	print(db.videogames.find(ob1, ob2).count())
   	else:    
       	__simple_print(db.videogames.find(ob1, ob2))


def q7(db, count=None):
	"""Find title of boardgames published by Avalon Games and playable in no more than
120 min"""
	
	what_find = {'publisher.name': 'Avalon Games', 'p_time': {'$lte': 120}}
	what_see  = {'_id': 0, 'title': 1}

   	if count:
       	print(db.boardgames.find(what_find, what_see).count())
   	else:
		__simple_print(db.boardgames.find(what_find, what_see))


def q8(db, count=None):
	"""Find title of videogames in which the second category is Action"""

	what_find = {'categories.1': 'Action'}
	what_see  = {'_id': 0, 'title': 1}
	
	if count:
		print(db.videogames.find(what_find, what_see).count())
	else:        
		__simple_print(db.videogames.find(what_find, what_see))


def q9(db, count=None):
	"""Find title and votes of videogames that received at least one evaluation of 8 or more"""

	what_find = {'ratings.vote': {'$gte': 8}}
	what_see  = {'_id': 0, 'title': 1, 'ratings.vote': 1}

	if count:
		print(db.videogames.find(what_find, what_see).count())
	else:        
		__simple_print(db.videogames.find(what_find, what_see))


def q10(db, count=None):
	"""Find titles and platforms of videogames that are sold by Amazon at less than 40"""

	what_find = {'vendors.name': 'Amazon', 'vendors.price': {'$lt': 40}}
	what_see  = {'_id': 0, 'title': 1, 'platforms': 1}

	if count:
		print(db.videogames.find(what_find, what_see).count())
	else:        
		__simple_print(db.videogames.find(what_find, what_see))


def q11(db, count=None):
	"""Find titles of videogames for which we have ratings"""
	
	what_find = {'ratings': {'$exists': 1}}
	what_see  = {'_id': 0, 'title': 1}

	if count:
	    print(db.videogames.find(what_find, what_see).count())
	else:        
	    __simple_print(db.videogames.find(what_find, what_see))


def q12(db, count=None):
	"""Find titles of boardgames where minimum number of players is between 2 and 4"""
	
   	what_find = {'min_players': {'$in': [2, 3, 4]}}
   	what_see  = {'_id': 0, 'title': 1}

   	if count:
       	print(db.boardgames.find(what_find, what_see).count())
   	else:
		__simple_print(db.boardgames.find(what_find, what_see))


def q13(db, count=None):
	"""Find average number of minimum and maximum players per year"""

	group = {'$group': {'_id': '$year',	'min_players': {'$avg': '$min_players'}, 
				'max_players': {'$avg': '$max_players'}}}

	pipeline = [group]

    if count:
       	print(db['games'].aggregate(pipeline).count())
    else:
		__simple_print(db['games'].aggregate(pipeline))


def q14(db, count=None):
	"""Calculate the number of Action games per year in the period 2010-
2015"""

	match = {'$match':  {'categories': {'$elemMatch': {'$eq': 'Action'}}, 
							'$and': [{'year': {'$gte': 2010}}, {'year': {'$lte': 2015}}]}}

	group    = {'$group': {'_id': {'year': '$year'}, 'count': {'$sum': 1}}}
	pipeline = [match, group]

   	if count:
       	print(db['games'].aggregate(pipeline).count())
   	else:
		__simple_print(db['games'].aggregate(pipeline))


def q15(db, count=None):
	"""Count games per category"""

	unwind 	 = {'$unwind': '$categories'} 
	group    = {'$group': {'_id': {'category': '$categories'}, 'count': {'$sum': 1}}}
	pipeline = [unwind, group]

    if count:
       	print(db['games'].aggregate(pipeline).count())
    else:
		__simple_print(db['games'].aggregate(pipeline))


def q16(db, count=None):
	"""Find years when more than 100 games have been published"""

	group 	 = {'$group': {'_id': {'year': '$year'}, 'total': {'$sum': 1}}}
	match 	 = {'$match': {'total': {'$gt': 100}}}
	project  = {'$project' :{'total': 0}}
	pipeline = [group, match, project]

    if count:
       	print(db['games'].aggregate(pipeline).count())
    else:
	   	__simple_print(db['games'].aggregate(pipeline))

	
def q17(db, count=None):
	"""Find distribution of categories per year"""

	unwind   = {'$unwind': '$categories'} 
	group    = {'$group': {'_id': {'year': '$year', 'category': '$categories'},
			     'how_many': {'$sum': 1}}}
	pipeline = [unwind, group]

	if count:
        print(db['games'].aggregate(pipeline).count())
    else:
		__simple_print(db['games'].aggregate(pipeline))


def cls():
	"""Clear screen"""
	os.system('clear')
