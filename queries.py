# coding=utf-8
from pymongo import *
from pprint import pprint
from json import loads, dumps
from os import system
import sys

db = MongoClient('localhost')['videogames']

Q1 = [ 
		{'$match': {'categories': 'Action', 
					'$and': [{'year': {'$gte': 2010}}, {'year': {'$lte': 2015}}]}},
		{'$group': { '_id': {'year': '$year'}, 'count': {'$sum': 1}}},
		{'$match': { 'count': {'$gt': 10}}}
]

Q2 = [ 
		{'$unwind': '$categories'},
		{'$group': {'_id': {'category': '$categories'}, 'count_games': {'$sum': 1}}}
]

Q3 = [ 
		{'$unwind': '$categories'},
		{'$group': {'_id': {'category': '$categories'}, 'games': {'$addToSet': '$_id'}}}	
]

Q4 = [
		{'$group': {'_id': {'year': '$year'}, 'count': {'$sum': 1}}},
		{'$match': {'count': {'$gt': 100}}},
		{'$project': {'year': 1}}
]

Q5 = [
		{'$unwind': '$categories'},
		{'$group': {'_id': {'year': '$year', 'category': '$categories'}, 'count': {'$sum': 1}}},
		{'$group': {'_id': '$_id.year', 'categories': {'$push': {'name': '$_id.category', 'count': '$count'}}}}
]


queries = [
	"+ a. Find year in which more than 10 Action games were produced between 2010 and 2015",
	"+ b. Count the number of games for each collection",
	"+ c. Create a collection with category as key and an array of games of the same category as value",
	"+ d. Find years in which more than 100 games were published",
	"+ e. Distribution of categories per year",
	"+ f. Clear screen",
	"+ g. Exit"
]


def clear():
	system('clear')
	
def show_queries():
	"""Shows all queries available"""

	print()
	print("*" * len(queries[2]))
	for q in queries:
		print(q)

	print("*" * len(queries[2]))
	print()

def output(query):
	"""Given a name of one of the available queries it outputs the corresponding result"""

	if query == 'g':
		print("bye")
		sys.exit()
	
	if query == 'a':
		query = Q1
		
	if query == 'b':
		query = Q2
	
	if query == 'c':
		query = Q3

	if query == 'd':
		query = Q4

	if query == 'e':
		query = Q5
	
	if query == 'f':
		system('clear')
		return
	
	cursor = db['games'].aggregate(query)
	
	for elem in cursor:
		pprint(dumps(elem))
		

	
#def main():
#	while True:
#		show_queries()
#		answer = str(input('Choose one of the above queries: '))
#		output(answer)

#main()
